package com.assignment.register;

// file Serve.java


import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;

import com.google.appengine.api.blobstore.BlobKey;
import com.google.appengine.api.blobstore.BlobstoreInputStream;
import com.google.appengine.api.datastore.DatastoreService;
import com.google.appengine.api.datastore.DatastoreServiceFactory;
import com.google.appengine.api.datastore.Entity;
import com.google.appengine.api.datastore.PreparedQuery;
import com.google.appengine.api.datastore.Query;

public class ServeServlet extends HttpServlet {
    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	//private BlobstoreService blobstoreService = BlobstoreServiceFactory.getBlobstoreService();

    @Override
    public void doGet(HttpServletRequest req, HttpServletResponse res)
        throws IOException {
            BlobKey blobKey = new BlobKey(req.getParameter("blob-key"));
            String keyString = blobKey.getKeyString();
            System.out.println(" keyString-- "+ keyString);
           // byte[] myFile = blobstoreService.fetchData(blobKey, 0, BlobstoreService.MAX_BLOB_FETCH_SIZE-1); 
                       
            String csv = getBlobAsString(keyString);
            PrintWriter out = res.getWriter();
            out.print(csv);
            
        }
    
    public static String getBlobAsString(String keyString) throws IOException {
        BlobKey blobKey = new BlobKey(keyString);
        String Values="",result="";
        int i=0,j=0;
        BlobstoreInputStream blobStream = new BlobstoreInputStream(blobKey);
        try {
			Workbook wb = WorkbookFactory.create(blobStream );
			Entity userRecord =null;
			Sheet sheet1 = wb.getSheet("Sheet1");      
			
	        java.util.Iterator<Row> ri = sheet1.rowIterator(); 
	        DatastoreService datastore = DatastoreServiceFactory.getDatastoreService();
	        while ( ri.hasNext() ) {
	        	//System.out.println("row:hasnext row------"+j++);
	        	Row row = ri.next();
	        	java.util.Iterator<Cell> ci = row.cellIterator();   
	        	if(j>=1) {
		        	userRecord = new Entity("User");
		        	System.out.println("here j:"+j);
		        	
	        	while ( ci.hasNext() ) {
	        		System.out.println("row:hasnext inner------");
	        		
	        		Cell cell = ci.next();
	        		 switch( cell.getCellType() ) {
	                 case Cell.CELL_TYPE_STRING:                             
	                   Values   = "'" + cell.getRichStringCellValue().getString() + "'";                
	                   break;
	                 case Cell.CELL_TYPE_NUMERIC:
	                   Values   = "'" + cell.getNumericCellValue() + "'";
	                   break;
	                 case Cell.CELL_TYPE_BOOLEAN:
	                   Values   = "'" + cell.getBooleanCellValue() + "'";
	                   break;
	                 case Cell.CELL_TYPE_FORMULA:
	                   Values   = "'" + cell.getCellFormula() + "'";
	                   break;
	                 default:
	                   Values   = "''";                
	                 } 
	        		 System.out.println("value----"+Values+" i:"+i);
	        		 if(i==0 && userRecord!=null) {
	        			 userRecord.setProperty("name",Values);
	        		 }
	        		 else if(i==1 && userRecord!=null){
	        			 userRecord.setProperty("gender",Values);
	        		 }
	        		 else if(i==2 && userRecord!=null){
	        			 userRecord.setProperty("email",Values);
	        		 }
	        		 else if(i==3 && userRecord!=null){
	        			 userRecord.setProperty("password",Values);
	        		 }
	        		 i++;
	        	}
	        }
	        	i=0;
	        	if(userRecord!=null){
	        	  datastore.put(userRecord);
	        	  System.out.println("in ds put------");
	        	}
	        	j=j+1;
	        }
	        //retreiving data
	        Query q = new Query("User");
	        PreparedQuery pq = datastore.prepare(q);
	        for (Entity entity : pq.asIterable()) 
	        {

	                String nm=(String)entity.getProperty("name");
	                String gen=(String)entity.getProperty("gender");
	                String email =(String)entity.getProperty("email");
	                String password =(String)entity.getProperty("password");
	                System.out.println("nm:"+nm+"~~"+"gen:"+gen+"~~"+"email:"+email+" pass:"+password);
	         }
	      result = "<html><body><h1>File Uploaded</h1><br/><a href='/login.jsp'>login</a></body></html>";
	        
		} catch (InvalidFormatException e) {
			// TODO Auto-generated catch block
			result="error while uploading";
			e.printStackTrace();
		}

       // return IOUtils.toString(blobStream, "UTF-8");
        return result;
    }
}