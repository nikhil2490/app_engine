package com.assignment.register;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

public class LoginServlet extends HttpServlet{
	
	  @Override
	    public void doPost(HttpServletRequest req, HttpServletResponse res)
	        throws ServletException,IOException {
		    
		    String un=req.getParameter("email");
			String pw=req.getParameter("pass");
			System.out.println("email:"+un+"  "+"pass:"+pw);
			//PrintWriter out = new PrintWriter();
			
			ValidateUser validateUser = new ValidateUser();
		    String username = validateUser.validateUser(un,pw);
			if(username.length()>0) {
				HttpSession session=req.getSession();  
		        session.setAttribute("uname",username);
				res.sendRedirect("/welcome.jsp");
				
			}
			else {
				res.sendRedirect("/error.html");
			}
			
	            
	        }

}
